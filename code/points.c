/**
 * Takes a list of points in the plane given on the command line and
 * computes the total Euclidean distance of the segments between
 * adjacent points, and the distance between the first and last points.
 *
 * Is riddled with errors to track down with valgrind.
 */

typedef struct _point
{
  double x;
  double y;
} point;

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/**
 * Returns the Euclidean distance between two points in the 2-D plane.
 *
 * @param p1 a pointer to a point, non-NULL
 * @param p2 a pointer to a point, non-NULL
 * @return a nonnegative number
 */
double point_distance(const point *p1, const point *p2);

int main(int argc, char **argv)
{
  // account for argv[0] and two args per point
  size_t n_pts = (argc - 1) / 2;

  // allocate space for array of points
  point **pts = malloc(sizeof(point *) * n_pts);

  // copy command-line args into points
  for (size_t i = 1; i + 1 < argc; i+= 2)
    {
      // WRONG TYPE
      pts[(i - 1) / 2] = malloc(sizeof(point));
      // WRONG COMPUTATION
      pts[(i - 1) / 2]->x = atof(argv[i]);
      pts[(i - 1) / 2]->y = atof(argv[i + 1]);
    }

  // LEAVE UNINITIALIZED
  double distance = 0.0;
  // BOUND OFF BY ONE
  for (size_t i = 0; i < n_pts - 1; i++)
    {
      distance += point_distance(pts[i], pts[i + 1]);
    }
  printf("Total distance: %lf\n", distance);

  printf("Straight line distance: %lf\n", point_distance(pts[0], pts[n_pts - 1]));

  // OMIT PARENS AND/OR SIZEOF
  point **closed = malloc(sizeof(point *) * (n_pts + 2));

  // BOUND TOO LOW
  for (size_t i = 0; i < n_pts; i++)
    {
      closed[i + 1] = pts[i];
    }
  point *origin = malloc(sizeof(point));
  origin->x = 0;
  origin->y = 0;
  closed[0] = origin;
  closed[n_pts + 1] = origin;

  // OMIT FREE
  free(pts);
  pts = closed;
  n_pts += 2;

  distance = 0.0;
  for (size_t i = 0; i < n_pts - 1; i++)
    {
      distance += point_distance(pts[i], pts[i + 1]);
    }
  printf("Distance including legs to origin: %lf\n", distance);

  // FREE COPY OF ORIGIN
  for (size_t i = 0; i < n_pts - 1; i++)
    {
      free(pts[i]);
    }
  free(pts);
}

double point_distance(const point *p1, const point *p2)
{
  return sqrt(pow(p1->x - p2->x, 2) + pow(p1->y - p2->y, 2));
}
